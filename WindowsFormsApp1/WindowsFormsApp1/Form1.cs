﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        //Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
        //znanstvenog kalkulatora, odnosno implementirati osnovne(+,-,*,/) i barem 5
        //naprednih(sin, cos, log, sqrt...) operacija.


        int br = 0;
        double rez = 0;
        string operacija = "";
        double trenutno = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "0";
            br = 0;
        }
        private void btn_1_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "1";
            br = 0;
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "2";
            br = 0;
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "3";
            br = 0;
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "4";
            br = 0;
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "5";
            br = 0;
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "6";
            br = 0;
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "7";
            br = 0;
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "8";
            br = 0;
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            if (rezultat.Text == "0")
                rezultat.Clear();
            rezultat.Text = rezultat.Text + "9";
            br = 0;
        }

        private void btn_CE_Click(object sender, EventArgs e)
        {
            rezultat.Text = "0";
            rez = 0;
            trenutno = 0;
        }

        private void btn_plus_Click(object sender, EventArgs e)
        {
            if (br == 0)
                rez += double.Parse(rezultat.Text);
            operacija = "+";
            rezultat.Text = "0";
            br++;
        }

        private void btn_minus_Click(object sender, EventArgs e)
        {
            if(br==0)
                rez = double.Parse(rezultat.Text);
            operacija = "-";
            rezultat.Text = "-";
            br++;

        }

        private void btn_multiply_Click(object sender, EventArgs e)
        {
            if (br == 0)
                rez = double.Parse(rezultat.Text);
            operacija = "*";
            rezultat.Text = "0";
            br++;
        }

        private void btn_devide_Click(object sender, EventArgs e)
        {
            if (br == 0)
                rez = double.Parse(rezultat.Text);
            operacija = "/";
            rezultat.Text = "0";
            br++;
        }

        private void btn_eq_Click(object sender, EventArgs e)
        {
            
            trenutno = double.Parse(rezultat.Text);
            if (operacija == "+")
                rezultat.Text = (rez + trenutno).ToString();
            if (operacija == "-")
                rezultat.Text = (rez + trenutno).ToString();
            if (operacija == "*")
                rezultat.Text = (rez * trenutno).ToString();
            if (operacija == "/")
            {
                if (trenutno == 0)
                    rezultat.Text = "NaN";
                else
                    rezultat.Text = (rez / trenutno).ToString();
            }
            rez = double.Parse(rezultat.Text);
            br = 0;
            rez = 0;
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            rez = Math.Sin(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            rez = Math.Cos(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_log_Click(object sender, EventArgs e)
        {
            rez = Math.Log10(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            rez = Math.Sqrt(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_abs_Click(object sender, EventArgs e)
        {
            rez = Math.Abs(double.Parse(rezultat.Text));
            rezultat.Text = rez.ToString();
        }

        private void btn_dot_Click(object sender, EventArgs e)
        {
            string comma = ",";
            string number = rezultat.Text.ToString();
            bool contains = number.Contains(comma);
            if(!contains)
                rezultat.Text = rezultat.Text + ",";
            br = 0;
        }
    }
}
