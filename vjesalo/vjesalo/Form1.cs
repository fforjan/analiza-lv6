﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vjesalo
{
    public partial class Form1 : Form
    {

        //Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke, i u
        //svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
        //funkcionalnost koju biste očekivali od takve igre.Nije nužno crtati vješala,
        //dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 



        int br = 6;
        string path = "C:\\rijeci.txt";
        List<string> rijeci = new List<string>();
        int trazenaRijecIndex;
        int pogoci;
        string trazenaRijec;
        string crtice;
        System.Text.StringBuilder crticeBuild;

        public Form1()
        {
            InitializeComponent();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }
            }
            NewGame();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_click(object sender, EventArgs e)
        {
           
            bool pogodak = false;
            pokusaji.Text = br.ToString();
            Button button = (Button)sender;
            button.Enabled = false;
            
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                
                if (trazenaRijec[i] == Char.ToLower(button.Text[0]))
                {
                    pogoci++;
                    crticeBuild[i * 2] = Char.ToLower(button.Text[0]);
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            rijec.Text = crtice;
            if (!pogodak)
            {
                br--;
                pokusaji.Text = br.ToString();
            }
            
            Pobjeda();
            Gubitak();
        }

        public void NewGame()
        {
            crtice = "";
            pogoci = 0;
            br = 6;
            Random rnd = new Random();
            trazenaRijecIndex = rnd.Next(0, rijeci.Count);
            trazenaRijec = rijeci[trazenaRijecIndex];
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                crtice += "_ ";
            }
            crticeBuild = new StringBuilder(crtice);
            pokusaji.Text = br.ToString();
            rijec.Text = crtice;
            foreach (Control c in Controls)
            {
                Button b = c as Button;
                if (b != null)
                {
                    b.Enabled = true;
                }
            }
        }

        public void Gubitak()
        {
            if (br <= 0)
            {

                MessageBox.Show($"Izgubili ste!\nRIJEC: {trazenaRijec}!","KRAJ!");
                NewGame();
            }
        }

        public void Pobjeda()
        {
            if (pogoci == trazenaRijec.Length)
            {
                MessageBox.Show("CESTITAMO!","POBJEDA!");
                NewGame();
            }
        }
        private void btn_new_Click(object sender, EventArgs e)
        {
            NewGame();
        }

       
    }
}
